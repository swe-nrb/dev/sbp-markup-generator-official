var fs = require('fs');
var Handlebars = require('handlebars');
var Promise = require('bluebird');

/**
 * This utility function scans a given directory for YAML-files and combines them to a JSON object
 * @param  {Object} settings  			A valid settingsobject with
 * @return {Object[] || String}     	A JSON object || A string to path 
 */
var step = function(settings) {
	return {
		settings: settings || {},
		run: run
	}
}

/**
 * This utility function scans a given directory for YAML-files and combines them to a JSON object
 * @param  {Object} pipeline  			A valid pipeline object with the key YAMLToJSONResult set
 * @return {Object[] || String}     	A JSON object || A string to path 
 */
function run(pipeline, settings) {
	_workDirPath = pipeline.getWorkDirPath() || './output_folder';
	_writeFolder = settings.writeFolder || true;
	_writeStore = settings.writeStore || true;
			
	return Promise
		.resolve()
		.then(readFilesAndCompileFiles(settings, pipeline, _writeFolder))
		.then(writeStream(_workDirPath, _writeFolder, pipeline))	
}

function readFilesAndCompileFiles(settings, pipeline) {
	return function () {
		/**
		 * Read json object from storage
		 */
		var items = pipeline.getKey('YAMLToJSONResult');

		// Load template
		try {
			var source = fs.readFileSync(settings.templatePath, "utf8");
			var template = Handlebars.compile(source);
		} catch(err) {
			if(err.code == 'ENOUT') {
				throw new Error(`Could not find template att path: ${settings.templatePath}`);
			} else {
				throw new Error(err);
			}
		}

		// populate template
		var md = template({ data: items });
		return md;
	}
}

function writeStream(_workDirPath, _writeFolder, pipeline) {
	return function (md) {
		if(_writeFolder) {
			// check if we have a directory to scan, else create it
			if (!fs.existsSync(_workDirPath)){
				try {
					fs.mkdirSync(_workDirPath);
				} catch(err){
					throw new Error('Could not create destination path to put files in ' + err.message);
				}
			}
			//Default write to workfolder
			try {
				fs.writeFileSync(_workDirPath + "/index.md", md);
			} catch(err){
				throw new Error('Could not write to file ' + err.message);
			}

			pipeline.setKey("markdownPath", _workDirPath + '/index.md');
			return 
		} 
		if(_writeStore) {
			pipeline.setKey("markdown", md);
			return
		} 
	}
}

module.exports.step = step;
module.exports.run = run;
