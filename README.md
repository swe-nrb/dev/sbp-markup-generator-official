Denna generator skapar märkspråk genom att läsa en eller flera YAML-filer och rendera innehållet enligt en tillhandahållen mall. Mallen renderas genom det populära `Handelbars` paketet. se deras [dokumentation](http://handlebarsjs.com/) för detaljer i hur mallen kan utformas.

## Förutsättningar

1. Data är strukturerat i en eller flera valida YAML-filer
2. YAML-filerna ligger lagrade i `./skript/data`
3. Mall är kompatibel med `Handlebars` och heter `dokument.md`
4. Mall är lagrat i `./skript/mallar`

## Installera

Installera generatorn i ditt informationspaket genom att skriva `npm install sbp-markup-generator-official --save`.